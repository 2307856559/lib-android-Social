package com.bookbuf.social.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by robert on 16/7/1.
 */
public class BitmapUtils {

	public static final String FOLDER = "umeng_cache";
	public static String PATH = "/mnt/sdcard/";
	public static final int COMPRESS_FLAG = 3145728;
	private static final String TAG = "BitmapUtils";
	private static final int MB = 1048576;
	private static final int FREE_SD_SPACE = 40;
	private static final int CACHE_SIZE = 10;
	public static int MAX_WIDTH;
	public static int MAX_HEIGHT;

	public BitmapUtils () {
	}

	public static void init () {
		boolean var0 = Environment.getExternalStorageState ().equals ("mounted");
		if (var0) {
			PATH = Environment.getExternalStorageDirectory ().getPath () + File.separator + "umeng_cache" + File.separator;
		} else {
			PATH = Environment.getDataDirectory ().getPath () + File.separator + "umeng_cache" + File.separator;
		}

		File var1 = new File (PATH);
		if (!var1.exists ()) {
			var1.mkdir ();
		}

		try {
			remove40PercentCache (PATH);
			var1 = null;
		} catch (Exception var3) {
			Log.d ("BitmapUtils", "清除缓存抛出异常 ： " + var3.toString ());
		}

		System.gc ();
	}

	private static BitmapFactory.Options getScaleBitmapOptions (String var0, int var1, int var2) {
		Log.d ("bitmapOptions", var0);
		InputStream var3 = getBitmapStream (var0);
		if (var3 == null) {
			return null;
		} else {
			BitmapFactory.Options var4 = new BitmapFactory.Options ();
			var4.inJustDecodeBounds = true;

			try {
				BitmapFactory.decodeStream (var3, (Rect) null, var4);
				int var5 = (int) Math.ceil ((double) (var4.outHeight / var2));
				int var6 = (int) Math.ceil ((double) (var4.outWidth / var1));
				if (var5 > 1 && var6 > 1) {
					if (var5 > var6) {
						var4.inSampleSize = var5;
					} else {
						var4.inSampleSize = var6;
					}
				}

				var4.inJustDecodeBounds = false;
			} catch (Exception var7) {
				var7.printStackTrace ();
			}

			closeInputStream (var3);
			return var4;
		}
	}

	public static InputStream getBitmapStream (String var0) {
		FileInputStream var1 = null;

		try {
			try {
				boolean var2 = true;
				if (var2) {
					var1 = new FileInputStream (new File (getFileName (var0)));
				}
			} catch (Exception var3) {
				var3.printStackTrace ();
			}

			if (var1 == null || var1.available () <= 0) {
				InputStream var5 = (new URL (var0)).openStream ();
				saveInputStream (getFileName (var0), var5);
				var1 = new FileInputStream (new File (getFileName (var0)));
			}
		} catch (Exception var4) {
			Log.e ("BitmapUtil", "读取图片流出错" + var4.toString ());
		}

		return var1;
	}

	private static void saveInputStream (String var0, InputStream var1) {
		FileOutputStream var2 = null;

		try {
			var2 = new FileOutputStream (new File (var0));
			byte[] var3 = new byte[1024];
			boolean var4 = false;

			int var18;
			while ((var18 = var1.read (var3)) != -1) {
				var2.write (var3, 0, var18);
			}

			var2.flush ();
		} catch (FileNotFoundException var15) {
			var15.printStackTrace ();
		} catch (IOException var16) {
			var16.printStackTrace ();
		} finally {
			if (var2 != null) {
				try {
					var2.close ();
				} catch (IOException var14) {
					;
				}
			}

		}

	}

	public static Bitmap loadImage (String var0, int var1, int var2) {
		Log.d ("loadImageUrl", var0);
		if (TextUtils.isEmpty (var0)) {
			return null;
		} else {
			Bitmap var3 = null;
			InputStream var4 = null;

			try {
				var4 = getBitmapStream (var0);
				var3 = BitmapFactory.decodeStream (var4, (Rect) null, getScaleBitmapOptions (var0, var1, var2));
			} catch (Exception var9) {
				var9.printStackTrace ();
			} finally {
				closeInputStream (var4);
			}

			return var3;
		}
	}

	public static boolean isFileExist (String var0) {
		if (TextUtils.isEmpty (var0)) {
			return false;
		} else {
			File var1 = new File (getFileName (var0));
			return var1.getAbsoluteFile ().exists ();
		}
	}

	public static boolean isNeedScale (String var0, int var1) {
		File var2 = new File (getFileName (var0));
		return var2.exists () && var2.length () >= (long) var1;
	}

	public static Bitmap getBitmapFromFile (String var0) {
		InputStream var1 = getBitmapStream (var0);
		if (var1 == null) {
			return null;
		} else {
			Bitmap var2 = BitmapFactory.decodeStream (var1, (Rect) null, (BitmapFactory.Options) null);
			closeInputStream (var1);
			return var2;
		}
	}

	public static Bitmap getBitmapFromFile (String var0, int var1, int var2) {
		InputStream var3 = getBitmapStream (var0);
		if (var3 == null) {
			return null;
		} else {
			Bitmap var4 = BitmapFactory.decodeStream (var3, (Rect) null, getScaleBitmapOptions (var0, var1, var2));
			closeInputStream (var3);
			return var4;
		}
	}

	public static void saveBitmap (String var0, Bitmap var1) {
		try {
			BufferedOutputStream var2 = new BufferedOutputStream (new FileOutputStream (getFileName (var0)));
			byte var3 = 100;
			if (var1.getRowBytes () * var1.getHeight () > 3145728) {
				var3 = 80;
			}

			var1.compress (Bitmap.CompressFormat.PNG, var3, var2);
			var2.flush ();
			var2.close ();
		} catch (Exception var4) {
			var4.printStackTrace ();
		}

	}

	private static void closeInputStream (InputStream var0) {
		if (var0 != null) {
			try {
				var0.close ();
			} catch (Exception var2) {
				Log.d ("BitmapUtils", var2.toString ());
			}
		}

	}

	public static String getFileName (String var0) {
		if (TextUtils.isEmpty (var0)) {
			return "";
		} else {
			String var1 = var0;
			if (var0.startsWith ("http://") || var0.startsWith ("https://")) {
				var1 = PATH + Md5Util.getMD5String (var0);
			}

			return var1;
		}
	}

	public static byte[] bitmap2Bytes (Bitmap var0) {
		ByteArrayOutputStream var1 = null;
		if (var0 != null && !var0.isRecycled ()) {
			try {
				var1 = new ByteArrayOutputStream ();
				int var2 = var0.getRowBytes () * var0.getHeight () / 1024;
				int var3 = 100;
				if ((float) var2 > 3072f) {
					var3 = (int) (3072f / (float) var2 * (float) var3);
				}

				Log.d ("BitmapUtil", "compress quality:" + var3);
				var0.compress (Bitmap.CompressFormat.JPEG, var3, var1);
				byte[] var4 = var1.toByteArray ();
				byte[] var5 = var4;
				return var5;
			} catch (Exception var15) {
				Log.e ("BitmapUtils", var15.toString ());
			} finally {
				if (var1 != null) {
					try {
						var1.close ();
					} catch (IOException var14) {
						;
					}
				}

			}

			return null;
		} else {
			Log.d ("BitmapUtils", "bitmap2Bytes  ==> bitmap == null or bitmap.isRecycled()");
			return null;
		}
	}

	public static int calculateInSampleSize (BitmapFactory.Options var0, int var1, int var2) {
		int var3 = var0.outHeight;
		int var4 = var0.outWidth;
		int var5 = 1;
		if (var3 > var2 || var4 > var1) {
			int var6 = var3 / 2;

			for (int var7 = var4 / 2; var6 / var5 > var2 && var7 / var5 > var1; var5 *= 2) {
				;
			}
		}

		return var5;
	}

	public static BitmapFactory.Options getBitmapOptions (byte[] var0) {
		BitmapFactory.Options var1 = new BitmapFactory.Options ();
		var1.inJustDecodeBounds = true;
		BitmapFactory.decodeByteArray (var0, 0, var0.length, var1);
		int var2 = (int) Math.ceil ((double) (var1.outWidth / MAX_WIDTH));
		int var3 = (int) Math.ceil ((double) (var1.outHeight / MAX_HEIGHT));
		if (var3 > 1 && var2 > 1) {
			if (var3 > var2) {
				var1.inSampleSize = var3;
			} else {
				var1.inSampleSize = var2;
			}
		} else if (var3 > 2) {
			var1.inSampleSize = var3;
		} else if (var2 > 2) {
			var1.inSampleSize = var2;
		}

		var1.inJustDecodeBounds = false;
		return var1;
	}

	private static int freeSpaceOnSd () {
		StatFs var0 = new StatFs (Environment.getExternalStorageDirectory ().getPath ());
		double var1 = (double) var0.getAvailableBlocks () * (double) var0.getBlockSize () / 1048576.0D;
		return (int) var1;
	}

	private static void remove40PercentCache (String var0) {
		File var1 = new File (var0);
		File[] var2 = var1.listFiles ();
		if (var2.length != 0) {
			int var3 = 0;

			int var4;
			for (var4 = 0; var4 < var2.length; ++var4) {
				var3 = (int) ((long) var3 + var2[var4].length ());
			}

			if (var3 > 10485760 || 40 > freeSpaceOnSd ()) {
				var4 = (int) (0.4D * (double) var2.length + 1.0D);
				Arrays.sort (var2, new BitmapUtils.FileLastModifSort ());

				for (int var5 = 0; var5 < var4; ++var5) {
					var2[var5].delete ();
				}
			}

		}
	}

	public static void cleanCache () {
		init ();
	}

	public static byte[] compressBitmap (byte[] var0, int var1) {
		boolean var2 = false;
		if (var0 != null && var0.length >= var1) {
			ByteArrayOutputStream var3 = new ByteArrayOutputStream ();
			Bitmap var4 = BitmapFactory.decodeByteArray (var0, 0, var0.length);
			int var5 = 1;
			double var6 = 1.0D;

			while (true) {
				while (!var2 && var5 <= 10) {
					var6 = Math.pow (0.8D, (double) var5);
					int var8 = (int) (100.0D * var6);
					Log.d ("BitmapUtils", "quality = " + var8);
					var4.compress (Bitmap.CompressFormat.JPEG, var8, var3);
					Log.d ("BitmapUtils", "WeiXin Thumb Size = " + var3.toByteArray ().length / 1024 + " KB");
					if (var3 != null && var3.size () < var1) {
						var2 = true;
					} else {
						var3.reset ();
						++var5;
					}
				}

				if (var3 != null) {
					byte[] var9 = var3.toByteArray ();
					if (!var4.isRecycled ()) {
						var4.recycle ();
					}

					if (var9 != null && var9.length <= 0) {
						Log.e ("BitmapUtils", "### 您的原始图片太大,导致缩略图压缩过后还大于32KB,请将分享到微信的图片进行适当缩小.");
					}

					return var9;
				}
				break;
			}
		}

		return var0;
	}

	public static Bitmap createThumb (Bitmap var0, float var1) {
		Bitmap var2 = null;
		if (var0 != null && !var0.isRecycled ()) {
			int var3 = var0.getWidth ();
			int var4 = var0.getHeight ();
			float var5 = 1.0F;
			if (var3 < 200 || var4 < 200) {
				if (var3 < var4) {
					var5 = var1 / (float) var3;
				} else {
					var5 = var1 / (float) var4;
				}
			}

			var2 = Bitmap.createScaledBitmap (var0, (int) (var5 * (float) var3), (int) (var5 * (float) var4), true);
			if (var2 == null) {
				var2 = var0;
			}
		}

		return var2;
	}

	static {
		init ();
		MAX_WIDTH = 768;
		MAX_HEIGHT = 1024;
	}

	private static class FileLastModifSort implements Comparator<File> {
		private FileLastModifSort () {
		}

		public int compare (File var1, File var2) {
			return var1.lastModified () > var2.lastModified () ? 1 : (var1.lastModified () == var2.lastModified () ? 0 : -1);
		}
	}
}
