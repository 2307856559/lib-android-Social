package com.bookbuf.social.util;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by robert on 16/6/30.
 */
public class Dummy {
	public Dummy () {
	}

	public static <T> T get (final Class<T> aClass, final T t) {
		if (t != null) {
			return t;
		} else if (!aClass.isInterface ()) {
			try {
				return aClass.newInstance ();
			} catch (Exception e) {
				return null;
			}
		} else {
			return (T) Proxy.newProxyInstance (
					aClass.getClassLoader (),
					new Class[]{aClass},
					new InvocationHandler () {

						@Override
						public Object invoke (Object proxy, Method method, Object[] args) throws Throwable {
							return null;
						}

					});
		}
	}
}
