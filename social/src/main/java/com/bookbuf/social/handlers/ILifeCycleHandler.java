package com.bookbuf.social.handlers;

import android.content.Context;
import android.content.Intent;

/**
 * 由Activity 调用
 */
public interface ILifeCycleHandler<Platform> {

	void onCreate (Context context, Platform platform);

	void onActivityResult (int requestCode, int resultCode, Intent intent);

	void onNewIntent (Intent intent);
}
