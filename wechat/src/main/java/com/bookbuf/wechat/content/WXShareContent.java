package com.bookbuf.wechat.content;

import android.text.TextUtils;

import com.bookbuf.social.exceptions.SocializeException;
import com.bookbuf.social.handlers.share.content.ShareContent;
import com.tencent.mm.sdk.openapi.WXMediaMessage;

/**
 * Created by robert on 16/6/30.
 */
public abstract class WXShareContent {

	private ShareContent shareContent;

	WXShareContent (ShareContent shareContent) {
		this.shareContent = shareContent;
	}

	public String getTarget () {
		return shareContent != null ? shareContent.mTargetUrl : null;
	}

	public ShareContent getShareContent () {
		return shareContent;
	}

	public abstract WXShareType getType ();

	public abstract WXMediaMessage build ();

	public static final class Constraint {
		public final int TITLE_LIMIT = 512;
		public final int DESCRIPTION_LIMIT = 1024;
	}

	public static class WXShareContentWrapper {
		private ShareContent shareContent;

		public WXShareContentWrapper (ShareContent shareContent) {
			this.shareContent = shareContent;
		}

		public WXShareContent build () {
			if (!TextUtils.isEmpty (this.shareContent.mText) && this.shareContent.mMedia == null) {
				return new WXText (shareContent);
			} else {
				throw new SocializeException ("there is no content to share.");
			}
		}
	}
}
